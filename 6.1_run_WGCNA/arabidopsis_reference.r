#WGCNA
library(WGCNA)
library(limmaDE2)
library(flashClust)
enableWGCNAThreads()
#allowWGCNAThreads()
options(stringsAsFactors = FALSE);

#---------------------Part 1 Data input and cleaning-----------------------------
setwd("/home/clingyun/CHEN/paper1/WGCNA/ara") # For Ya'old workstation

rlog_matrix <- read.table(file = "reference_salmon_TPM", sep = "\t", header = TRUE)
row.names(rlog_matrix) <- rlog_matrix[,1]
rlog_matrix[1:2,] #show the first two row of matrix
rlog_matrix <- rlog_matrix[,-c(1,7)]
rlog_matrix[1:2,] #show the first two row of matrix
rlog_matrix <- rlog_matrix[rowSums(rlog_matrix) > 0, ] # remove the row with all the sample = 0 

datExpr0 = as.data.frame(t(rlog_matrix));

# 1.b Chekcing for excessiving missing values and identification of outlier microarray samples
# Check for genes and samples with too many missing values
gsg = goodSamplesGenes(datExpr0, verbose = 3);
gsg$allOK

# If the last command return TRUE, all genes passed the cuts. Otherwise, removing the offending genes and samples using following commands
if (!gsg$allOK)
{
# Optionally, print the gene and sample names that were removed:
if (sum(!gsg$goodGenes)>0)
printFlush(paste("Removing genes:", paste(names(datExpr0)[!gsg$goodGenes], collapse = ", ")));
if (sum(!gsg$goodSamples)>0)
printFlush(paste("Removing samples:", paste(rownames(datExpr0)[!gsg$goodSamples], collapse = ", ")));
# Remove the offending genes and samples from the data:
datExpr0 = datExpr0[gsg$goodSamples, gsg$goodGenes]
}

sampleTree = hclust(dist(datExpr0), method = "average");
# Plot the sample tree: Open a graphic output window of size 12 by 9 inches
# The user should change the dimensions if the window is too large or too small.
pdf(file = "/home/clingyun/CHEN/paper1/WGCNA/ara/arabidopsis_reference_sampleTree.pdf", width = 12, height = 9); # Save in Ya server
sizeGrWindow(9,5)
#pdf(file = "C:/cygwin64/home/FemaleLiver-Data/Plots/sampleClustering.pdf", width = 12, height = 9); # You can save the pdf
par(cex = 0.6);
par(mar = c(0,4,2,0))
plot(sampleTree, main = "Sample clustering to detect outliers", sub="", xlab="", cex.lab = 1.5,cex.axis = 1.5, cex.main = 2)


#**Remove the sampling with any obvious outliers
abline(h = 15000, col = "red");
# Determine cluster under the line
clust = cutreeStatic(sampleTree, cutHeight = 95000, minSize = 10)
table(clust)
# clust 1 contains the samples we want to keep.
keepSamples = (clust==1)
datExpr = datExpr0[keepSamples, ]
nGenes = ncol(datExpr)
nSamples = nrow(datExpr)
dim(datExpr)
rownames(datExpr)

# If no need to remove
datExpr = datExpr0

# Now datExpr can be used for gene co-expression analysis

# Save data for next step
save(datExpr, file = "reference_arabidopsis.RData")

# -------------Part  2  Network construction and module detection------------------------------------------
# 2.0 Loading data from last step
lnames = load(file = "reference_arabidopsis.RData");
lnames
# -------2.a Automatic network construction and module detection
# 2.a.1 Choosing the soft-thresholding power: analysis of network topology
powers = c(c(1:10), seq(from = 12, to=20, by=2))
# Call the network topology analysis function
sft = pickSoftThreshold(datExpr, powerVector = powers, verbose = 5)
# Plot the results:
sizeGrWindow(9, 5)
par(mfrow = c(1,2));
cex1 = 0.9;
# Scale-free topology fit index as a function of the soft-thresholding power
plot(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2],
xlab="Soft Threshold (power)",ylab="Scale Free Topology Model Fit,signed R^2",type="n",
main = paste("Scale independence"));
text(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2],
labels=powers,cex=cex1,col="red");
# this line corresponds to using an R^2 cut-off of h
abline(h=0.70,col="red")
# Mean connectivity as a function of the soft-thresholding power
plot(sft$fitIndices[,1], sft$fitIndices[,5],
xlab="Soft Threshold (power)",ylab="Mean Connectivity", type="n",
main = paste("Mean connectivity"))
text(sft$fitIndices[,1], sft$fitIndices[,5], labels=powers, cex=cex1,col="red")

# 2.a.2 One step network construction and module detection
net = blockwiseModules(datExpr,power = 6,TOMType = "unsigned", minModuleSize = 30,reassignThreshold = 0, mergeCutHeight = 0.25,numericLabels = TRUE, pamRespectsDendro = FALSE,saveTOMs = TRUE,saveTOMFileBase = "reference_TOM",verbose = 3)

table(net$colors)

sizeGrWindow(12, 9)
# Convert labels to colors for plotting
mergedColors = labels2colors(net$colors)
# Plot the dendrogram and the module colors underneath
plotDendroAndColors(net$dendrograms[[1]], mergedColors[net$blockGenes[[1]]],
"Module colors",
dendroLabels = FALSE, hang = 0.03,
addGuide = TRUE, guideHang = 0.05)

moduleLabels = net$colors
moduleColors = labels2colors(net$colors)
MEs = net$MEs;
geneTree = net$dendrograms[[1]];
save(net, MEs, moduleLabels, moduleColors, geneTree, file = "reference-networkautoConstruction-auto.RData")

lnames = load(file = "reference-networkautoConstruction-auto.RData");
lnames

# 3 correlate module to trait/treatment
traitData0 <- read.table(file = "arabidpsis_wgcna_trait.txt", sep = "\t", header = TRUE)
row.names(traitData0) <- traitData0[,1]
value <- traitData0[,-1]
traitData <- as.matrix(value, format = TRUE, rowLabels = TRUE, colLabels = TRUE)
row.names(traitData) <- row.names(traitData0)
colnames(traitData) <- 'value'

MEs0 = moduleEigengenes(datExpr, moduleColors)$eigengenes
MEs = orderMEs(MEs0)
moduleTraitCor = cor(MEs, traitData, use = "p")
moduleTraitCor

nSamples = nrow(datExpr)
moduleTraitPvalue = corPvalueStudent(moduleTraitCor, nSamples)
moduleTraitPvalue

# get and export hub genes
datKME = signedKME(datExpr, MEs)
rownames(datKME)[datKME[,1] >0.9] # Get gene ID of kme value >0.9, for the 1 column module. Get the hub genes

for (i in colnames(datKME))
write.table(rownames(datKME)[datKME[,i] >0.9],file = paste('reference_', i, sep =''))
