"""Calculate the percent identity for the two transcripts within each False Positive or False Negative in pairwise combination of evaluating clustering"""

import sys,os
import itertools

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: calculate_identity_FN_FP.py blat.psl list_FP/list_FN"
		sys.exit(0)
	blat_file = open(sys.argv[1],'r') # query is de novo transcripts, target is also de novo transcripts
	list_FP_FN = open(sys.argv[2], 'r') # cluster_file
	blat_file = blat_file.readlines()
	list_FP_FN = list_FP_FN.readlines()
	identity_dict = {} # key is the pair of query_target, value is the identity value
	identity_list = []
	name = os.path.basename(sys.argv[2])
	print 'processing', name 
	for line in blat_file:
		if len(line) < 3: continue #ignore empty lines
		spls = line.strip().split('\t')
		identity_dict['_'.join([spls[0], spls[1]])] = float(spls[2])/100.0
	#print identity_dict
	for line in list_FP_FN:
		if len(line) < 3: continue #ignore empty lines
		spls = line.strip().split(',')
		spls0,spls1 =spls[0].strip('\(\'\) '), spls[1].strip(' \'\)')
		pair = '_'.join([spls0,spls1])
		#print pair
		if pair in identity_dict:
			print pair, identity_dict[pair]
			identity_list.append(identity_dict[pair])
	
	with open(name + "_identity_list", "a") as outfile:
		for xxxx in identity_list:
			outfile.write(str(xxxx)+ '\n')
	
	cmd = ""
	cmd += "setwd(\"" + os.path.abspath(os.curdir) + "\")"+ '\n'
	cmd += "pdf=pdf(file='" + name + "_identity_distribution.pdf',width=7,height=6)" + '\n'
	cmd += "b <- read.table('" + name + "_identity_list" + "')" + '\n'
	cmd += "hist(b[,1],breaks=50,col=rgb(0,0.5,0,0.5))"+ '\n'
	cmd += "dev.off()"+ '\n'

	if os.path.isfile('plot_identity.r'):
		os.system('rm plot_identity.r')
	with open("plot_identity.r", "a") as outfile:
			outfile.write(str(cmd)+ '\n')