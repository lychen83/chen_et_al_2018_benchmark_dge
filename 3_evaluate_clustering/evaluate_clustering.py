"""Evaluate clustering methods Trinity, Corset, Grouper, CD-HIT-EST. using pairwise combinations of all de novo transcripts"""

import sys,os
import itertools

SIMILARITY_CUTOFF = 0.98 #only consider HSPs >= this percentage similarity
MATCH_CUTOFF = 100 #only consider HSPs with # matches >= this


def process_cluster(cluster_infor):
	# Using the cluster information, get correspondence between cluster and gene
	print 'proces cluster information'
	global true_queries_genes_cluster_dict, query_gene_score_dict
	cluster_infor = cluster_infor.readlines()
	cluster_dict = {} # such as {'cluster1':['Tran1','Trans2'],'cluster2:'....}
	for line in cluster_infor:
		if len(line) < 3: continue #ignore empty lines
		spls = line.strip().split('\t')
		if spls[0] in chimera_id: continue
		if spls[1] not in cluster_dict: 
			cluster_dict[spls[1]] = [spls[0]]
		else: cluster_dict[spls[1]].append(spls[0]) # such as {'cluster1':['Tran1','Trans2'],'cluster2:'....}
	num_cluster_software = len(cluster_dict)
	
	return num_cluster_software

def process_block(block):
	"""take a block of all blat output lines for one query and check"""
	if len(block) == 0:
		pass #no line passed the filters. Ignore the query
	elif len(block) == 1:
		best_hit = block[0]
		return best_hit
	else: #multiple blat his
		best_hit = block[0] 
		for hit in block[1:]: #find the hit with max blat score
			if hit[11] > best_hit[11]: best_hit = hit
		return best_hit

def process_ambigous_id(blat_output_lines,bitscore_ratio):
	query_score_dict = {} #query is transcript, key is bitscore value (from different target genes)
	for line in blat_output_lines:	
		#Parse data
		if len(line) < 3: continue #ignore empty lines
		spls = line.strip().split('\t')
		if '.' in spls[1]:  # some reference annotation such as arabidopsis include multiple isoforms (AT5G67610.2, AT5G67610.1). Ignore the .1, .2..
			spls[1] = str(spls[1].split('.')[0])
		if spls[0] in chimera_id: continue
		for column in [2,3,4,5,6,7,8,9,11]:
			spls[column] = float(spls[column]) #fix data type
		similarity = spls[2] 
		score = spls[11]
		match = spls[3]
		query = spls[0]
		if query not in query_score_dict:
			query_score_dict[query] = [score]
			last_match = spls[3]
		else:
			if match != last_match:
				query_score_dict[query].append(score)
	ambiguous_id = []
	for xx in query_score_dict:
		query_score_dict[xx] = sorted(query_score_dict[xx],reverse = True)
	for xx in query_score_dict:
		if len(query_score_dict[xx]) >= 2:
			ratio = query_score_dict[xx][1]/query_score_dict[xx][0]
			#print ratio, bitscore_ratio
			#print xx
			if ratio > float(bitscore_ratio):
				ambiguous_id.append(xx)
	print 'ambiguous_id', len(ambiguous_id)
	return ambiguous_id
		
if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: Evaluate_clustering.py blat_contig_reference de_novo_clusters chimera_id bitscore_ratio"
		sys.exit(0)
	blat_file = open(sys.argv[1],'r') # query is de novo transcripts, target is reference genes
	clusters = open(sys.argv[2], 'r') # cluster_file
	chimera_file = open(sys.argv[3], 'r') # a file incluldes the ids of chimeric transcripts
	bitscore_ratio = sys.argv[4] # the bit score ratio used to exclude the ambiguously assigned transcripts
	name = os.path.basename(sys.argv[2])
	# 1 get true clustering. Key is contig ID, value is ref gene ID
	print "process file "+sys.argv[2]
	print "getting the true cluster infor"
	blat_output_lines = blat_file.readlines()
	chimera_file = chimera_file.readlines()
	chimera_id = []
	for chimera in chimera_file:
		chimera_id.append(chimera.strip())

	if 'psLayout version 3' in blat_output_lines[0]: # ignor the title of the blat results
		blat_output_lines = blat_output_lines[5:]
	block = [] #all the hits (reference) belong to one query (de novo)
	last_query = ""
	true_queries_genes_cluster_dict = {} #key is query, value is its matched reference gene with highest score
	true_genes_queries_cluster_dict = {} #key is reference gene, value is query
	query_gene_score_dict = {} # key are transcript, value is the highest score between query and its matched reference genes
	
	ambiguous_id = process_ambigous_id(blat_output_lines, bitscore_ratio)
	
	for line in blat_output_lines:	
		#Parse data
		if len(line) < 3: continue #ignore empty lines
		spls = line.strip().split('\t')
		if 'GSVIVT' in spls[1]:
			spls[1] = spls[1].replace('GSVIVT','GSVIVG')
		if '.' in spls[1]:  # some reference annotation such as arabidopsis include multiple isoforms (AT5G67610.2, AT5G67610.1). Ignore the .1, .2..
			spls[1] = str(spls[1].split('.')[0])
		if spls[0] in chimera_id: continue
		if spls[0] in ambiguous_id: continue
		for column in [2,3,4,5,6,7,8,9,11]:
			spls[column] = float(spls[column]) #fix data type
		similarity = spls[2]
		score = spls[11]
		query = spls[0]
		match = spls[3]
		
		#Get blocks with the same query
		if similarity >= SIMILARITY_CUTOFF and match >= MATCH_CUTOFF:
			if query == last_query or block == []:
				block.append(spls)#add to block	
			else:   # new query
				best_hit_2 = process_block(block)#send the whole block off  Chen: whole block from last query--target
				query_gene_score_dict[best_hit_2[0]] = best_hit_2[11]
				if best_hit_2[0] not in true_queries_genes_cluster_dict:
					true_queries_genes_cluster_dict[best_hit_2[0]] = best_hit_2[1]
				else:
					print 'error happen', sys.exit()
				block = [spls]#reset and initiate the block.  Chen: set this new query--target as block
			last_query = query
	best_hit_2 = process_block(block)#process the last block
	query_gene_score_dict[best_hit_2[0]] = best_hit_2[11]
	true_queries_genes_cluster_dict[best_hit_2[0]] = best_hit_2[1]

	
	for k, v in true_queries_genes_cluster_dict.items():
		true_genes_queries_cluster_dict.setdefault(v, []).append(k)

	num_cluster_software = process_cluster(clusters)  # key is cluster id, value is reference gene
	
	# 2 compare de novo clustering infor
	print "Getting cluster infor"
	num_TP, num_FP, num_TN, num_FN = 0,0,0,0
	clusters = open(sys.argv[2], 'r') # cluster_file
	clusters = clusters.readlines()
	software_queries_genes_cluster_dict = {} # key is contig, value is cluster ID
	software_genes_queries_cluster_dict = {} # key is cluster ID, value is contigs (contigs which have hit from reference)
	for line in clusters:
		if len(line) < 3: continue #ignore empty lines
		contig, gene = line.split("\t")[0].strip(), line.split("\t")[1].strip()
		if contig not in true_queries_genes_cluster_dict: continue
		software_queries_genes_cluster_dict[contig] = gene
		if gene not in software_genes_queries_cluster_dict:
			software_genes_queries_cluster_dict[gene] = [contig]
		else:
			software_genes_queries_cluster_dict[gene].append(contig)

	# calculate mean number of transcripts in each cluster, both true and software 
	num_total_true_transcripts = 0
	for yy in true_genes_queries_cluster_dict.values():
		for zz in yy: 
			if len(zz) >1: num_total_true_transcripts +=1
	mean_num_true_clustering = float(num_total_true_transcripts)/float(len(true_genes_queries_cluster_dict.keys()))

	num_total_software_clustering_transcripts = 0
	for mm in software_genes_queries_cluster_dict.values():
		for nn in mm: 
			if len(nn) >1: num_total_software_clustering_transcripts +=1
	mean_num_software_clustering = float(num_total_software_clustering_transcripts)/float(len(software_genes_queries_cluster_dict.keys()))
	
	print 'number of reference gene which include transcripts', len(true_genes_queries_cluster_dict)
	print 'number of evaluated clusters', len(software_genes_queries_cluster_dict)
	print 'by mapping transcripts to reference, average transcripts in each reference gene is', mean_num_true_clustering
	print 'by software clustering, average transcripts in cluster is', mean_num_software_clustering
	

###################################################################################################
############################### Calculating Correct clusters#######################################
###################################################################################################
	num_correct_clusters = 0
	num_incorrect_clusters = 0
	num_incorrect_clusters_1 = 0
	num_incorrect_clusters_2 = 0
	correct_clusters_list = [] 
	for a in software_genes_queries_cluster_dict:
		mark = []
		for xyz in software_genes_queries_cluster_dict[a]:
			if true_queries_genes_cluster_dict[xyz] not in mark:
				mark.append(true_queries_genes_cluster_dict[xyz])
		if len(mark) == 1 and len(software_genes_queries_cluster_dict[a]) == len(true_genes_queries_cluster_dict[true_queries_genes_cluster_dict[xyz]]):
			num_correct_clusters +=1
		if len(mark) >1:
			num_incorrect_clusters_1 +=1
		if len(mark) == 1 and len(software_genes_queries_cluster_dict[a]) != len(true_genes_queries_cluster_dict[true_queries_genes_cluster_dict[xyz]]):
			num_incorrect_clusters_2 +=1
		if len(mark) == 0:
			print 'error happen'
	num_incorrect_clusters = num_incorrect_clusters_1 + num_incorrect_clusters_2
	

	print 'nummber of total software clusters (excluded chimeric) ', num_cluster_software
	print 'num_correct_clusters', num_correct_clusters
	print 'num_incorrect_clusters', num_incorrect_clusters
	print 'num_incorrect_clusters_1', num_incorrect_clusters_1
	print 'num_incorrect_clusters_2', num_incorrect_clusters_2
	print 'proportion of incorrect clusters due to error 1', float(num_incorrect_clusters_1)/len(software_genes_queries_cluster_dict)
	print 'proportion of incorrect clusters due to error 2', float(num_incorrect_clusters_2)/len(software_genes_queries_cluster_dict)

	"""############################### Evaluating using pairwise combination############################### 
	combinations = itertools.combinations(software_queries_genes_cluster_dict,2)
	print 'total number of contig evaluated', len(software_queries_genes_cluster_dict)
	print "evaluating pairwise combination of all contigs"
	
	FP_list = []
	FN_list = []
	for a in combinations:
		# True positive
		if software_queries_genes_cluster_dict[a[0]] == software_queries_genes_cluster_dict[a[1]] \
		and true_queries_genes_cluster_dict[a[0]] == true_queries_genes_cluster_dict[a[1]]:
			num_TP +=1
		# False positive
		if software_queries_genes_cluster_dict[a[0]] == software_queries_genes_cluster_dict[a[1]] \
		and true_queries_genes_cluster_dict[a[0]] != true_queries_genes_cluster_dict[a[1]]:
			num_FP += 1
			FP_list.append(a)
		# True negative
		if software_queries_genes_cluster_dict[a[0]] != software_queries_genes_cluster_dict[a[1]] \
		and true_queries_genes_cluster_dict[a[0]] != true_queries_genes_cluster_dict[a[1]]:
			num_TN += 1
		# False negative
		if software_queries_genes_cluster_dict[a[0]] != software_queries_genes_cluster_dict[a[1]] \
		and true_queries_genes_cluster_dict[a[0]] == true_queries_genes_cluster_dict[a[1]]:
			num_FN += 1
			FN_list.append(a)
	
	
	print "TP, FP, TN, FN", num_TP, num_FP, num_TN, num_FN
	print 'Precision, Recall', float(num_TP)/(num_TP+num_FP), float(num_TP)/(num_TP+num_FN)
	print ''
	
	with open(name +"_evaluate_results" + str(bitscore_ratio),"a") as outfile:
		outfile.write('len(ambiguous_id)' + str(ambiguous_id)+ '\n')
		outfile.write("TP, FP, TN, FN" + ' ' + str(num_TP) + ' ' + str(num_FP) + ' ' + str(num_TN) + ' ' + str(num_FN) +'\n')
		outfile.write('Precision, Recall' + ' ' + str(float(num_TP)/(num_TP+num_FP)) + ' ' + str(float(num_TP)/(num_TP+num_FN)) +'\n')
		outfile.write('num_correct_clusters' + ' ' + str(num_correct_clusters) +'\n')
		outfile.write('num_incorrect_clusters' + ' ' + str(num_incorrect_clusters) +'\n')
		outfile.write('num cluster_gene_dict' + ' ' + str(len(cluster_gene_dict))+'\n')
		outfile.write("TP, FP, TN, FN" + str(num_TP) + str(num_FP)+str(num_TN)+str(num_FN))
	with open(name + "_FP_list", "a") as outfile:
		for xxxx in FP_list:
			outfile.write(str(xxxx)+ '\n')
	with open(name + "_FN_list", "a") as outfile:
		for yyyy in FN_list:
			outfile.write(str(yyyy)+ '\n')
			"""
