"""identify homologs within a species"""

import sys,os

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python find_homologs_within_species.py num_cores species.pep.fa"
		sys.exit(0)
	
	num_cores,species = sys.argv[1:]
	cmd = "makeblastdb -in "+species+" -parse_seqids -dbtype prot -out "+species
	print cmd
	os.system(cmd)
	cmd = "blastp -db "+species+" -query "+species+" -evalue 10 -num_threads "+str(num_cores)
	cmd += " -max_target_seqs 20 -out "+species+".rawblastp "
	cmd += "-outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'"
	print cmd
	os.system(cmd)
	print cmd
	os.system(cmd)
	
	if not os.path.exists(species+".blastp.filtered"):
		block = [] # store a list of query,hit pairs with the same query
		infile = open(species+".rawblastp","r")
		outfile = open(species+".blastp.filtered","w")
		last_query,last_hit = "",""
		for line in infile:
			spls = line.split("\t")
			if len(spls) < 3: continue
			query,hit,pident,nident = spls[0],spls[2],float(spls[5]),int(spls[6])
			if query == hit or pident < 20.0 or nident < 50: continue
			if query == last_query or block == []:
				if (query,hit) not in block:
					block.append((query,hit)) # add tuple to block
			else:
				if len(block) < 10:
					for i in block:
						outfile.write(i[0]+"\t"+i[1]+"\n")
				block = []#reset and initiate the block
				block.append((query,hit))	
			last_query,last_hit = query,hit
		# process the last block
		if len(block) < 10:
			for i in block:
				outfile.write(i[0]+"\t"+i[1]+"\n")
		block = []#reset and initiate the block
		infile.close()
		outfile.close()