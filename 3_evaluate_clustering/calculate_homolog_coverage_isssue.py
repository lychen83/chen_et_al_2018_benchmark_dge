"""Estimate the proportion of false negatives due to homolog issue or coverage issue"""

import sys,os
import itertools

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: calculate_homolog_coverage_issue.py blat.psl list_FP/list_FN min_match_length"
		sys.exit(0)
	blat_file = open(sys.argv[1],'r') # query is de novo transcripts, target is also de novo transcripts
	list_FP_FN = open(sys.argv[2], 'r') # cluster_file
	blat_file = blat_file.readlines()
	list_FP_FN = list_FP_FN.readlines()
	min_aligned_length = sys.argv[3]
	identity_dict = {} # key is the pair of query_target, value is the match length
	matched = 0
	unmatched = 0
	name = os.path.basename(sys.argv[2])
	print 'processing', name 
	for line in blat_file:
		if len(line) < 3: continue #ignore empty lines
		spls = line.strip().split('\t')
		if float(spls[3]) >= float(min_aligned_length) and float(spls[2]) >=90: 
			identity_dict['_'.join([spls[0], spls[1]])] = float(spls[3])
	for line in list_FP_FN:
		if len(line) < 3: continue #ignore empty lines
		spls = line.strip().split(',')
		spls0,spls1 =spls[0].strip('\(\'\) '), spls[1].strip(' \'\)')
		pair = '_'.join([spls0,spls1])
		if pair in identity_dict:
			matched +=1
		else:
			unmatched +=1
	print 'the matched are', matched
	print 'the unmatched are', unmatched
	print 'the proportion matched/(matched + unmatched)', float(matched)/(matched + unmatched)
