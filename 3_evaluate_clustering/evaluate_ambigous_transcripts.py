"""Evaluating if the ambiguous assigned transcripts are homologs or not"""

import sys,os

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python evaluate_ambiguous_transcripts.py blat_output(target is reference genes) *filtered bitscore_ratio"
		sys.exit(0)
	
	blat_output_lines,blastp_homologs, bitscore_ratio = sys.argv[1:]
	blat_output_lines = open(sys.argv[1],'r')
	blastp_homologs = open(sys.argv[2],'r')
	
	blat_output_lines = blat_output_lines.readlines()
	blastp_homologs = blastp_homologs.readlines()
	query_score_dict = {} #query is transcript, key is bitscore value (from different target genes)
	query_target_dict = {} #key is transcript, target is a also a dict: key is the matched reference gene name, value is bit score.
	for line in blat_output_lines:	
		#Parse data
		if len(line) < 3: continue #ignore empty lines
		spls = line.strip().split('\t')
		if '.' in spls[1]:  # some reference annotation such as arabidopsis include multiple isoforms (AT5G67610.2, AT5G67610.1). Ignore the .1, .2..
			spls[1] = str(spls[1].split('.')[0])
		if '_' in spls[1]:
			spls[1] = str(spls[1].split('_')[0])
		for column in [2,3,4,5,6,7,8,9,11]:
			spls[column] = float(spls[column]) #fix data type
		similarity = spls[2] 
		score = float(spls[11])
		target = spls[1]
		query = spls[0]
		if query not in query_score_dict:
			query_score_dict[query] = [score]
			query_target_dict[query] = {target:score}
			last_match = spls[1]
		else:
			if target != last_match:
				query_score_dict[query].append(score)
				query_target_dict[query].update({target:score})
	ambiguous_id = []
	ambiguous_id_paralog = []
	for xx in query_score_dict:
		query_score_dict[xx] = sorted(query_score_dict[xx],reverse = True)
	
	blastp_homologs_pairs = []
	for line in blastp_homologs:
		if len(line) < 3: continue #ignore empty lines
		spls = line.strip().split('\t')
		split0 = spls[0].split('.')[0]
		split1 = spls[1].split('.')[0]
		
		blastp_homologs_pairs.append(''.join([split0,split1]))
		blastp_homologs_pairs.append(''.join([split1,split0]))
	print blastp_homologs_pairs
	for xx in query_score_dict:
		if len(query_score_dict[xx]) >= 2:
			ambigous_target_list = []
			ratio = query_score_dict[xx][1]/query_score_dict[xx][0]
			#print ratio, bitscore_ratio
			#print xx
			if ratio > float(bitscore_ratio):
				#print query_target_dict[xx]
				for xy in query_target_dict[xx]: # is a dict like {query:{reference_gene1:bitscore, reference_gene2:bitsocre}}
						#print xy
						if float(query_target_dict[xx][xy]) == query_score_dict[xx][1] or float(query_target_dict[xx][xy]) == query_score_dict[xx][0]:
							ambigous_target_list.append(xy)
							#print ''.join(ambigous_target_list)
							if ''.join(ambigous_target_list) in blastp_homologs_pairs:
								ambiguous_id_paralog.append(xx)
				ambiguous_id.append(xx)
				
	print 'ambiguous_id', len(ambiguous_id)
	print 'ambiguous_id_paralog', len(ambiguous_id_paralog)
	print 'percentage of ambiguous_due_to_paralogs', float(len(ambiguous_id_paralog))/len(ambiguous_id)