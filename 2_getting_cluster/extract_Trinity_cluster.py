"""This script was used to extract cluster information in 'Trinity.fasta' generatd by software Trinity. It was resived from https://github.com/Oshlack/Lace/wiki/Example%3A-Differential-Transcript-Usage-on-a-non-model-organism
# Header of fasta is like: >TRINITY_DN12414_c0_g3_i1 len=260 path=[391:0-259] [-1, 391, -2]
"""

import sys
if(len(sys.argv) < 2):
    print("Please include name of Trinity fasta file you wish to parse")
    sys.exit()
f = open(sys.argv[1],'r') #open file
fw = open('Trinity_extracted_clusters.txt','w') #open file to write to

for line in f:
    if('>' in line):
            contig_name = line.split()[0].lstrip('>')
            temp = (line.lstrip('>')).split()[0].split('_')
            cluster_name = "_".join([temp[0],temp[1],temp[2],temp[3]])
            fw.write(contig_name + "\t" + cluster_name + "\n")

    else:
            continue

fw.close()
f.close()