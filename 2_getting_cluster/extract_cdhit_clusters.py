"""this script was used to extract cluster information from cd-hit output *.clstr"""

import sys

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: extract_clusters_cdhit *clstr"
		sys.exit(0)
	clusters = open(sys.argv[1], 'r')
	num_clusters = 0
	num_contigs = 0
	clusters_dict = {}
	#Get the gene list for the de novo result
	for line in clusters.readlines():
		if len(line) < 2: continue #ignore the empty lines
		if '>Cluster' in line:
			cluster_ID = line.strip().replace('>','')
			cluster_ID = cluster_ID.replace(' ','_')
			clusters_dict[cluster_ID] = []
			num_clusters +=1
		else:
			if '...' in line:
				contig_ID = line.split('>')[1].split('...')[0]
				contig_ID = 'TRINITY_DN' + contig_ID
				clusters_dict[cluster_ID].append(contig_ID)
				num_contigs += 1
	print 'number of clusters is', num_clusters
	print 'number of clusters is', len(set(clusters_dict))
	with open("cd_hit_clusters.txt","a") as outfile:
		for i in clusters_dict:
			for contig in clusters_dict[i]:
				outfile.write(contig +"\t" + i + "\n")
	
		
		
		
		
		

		