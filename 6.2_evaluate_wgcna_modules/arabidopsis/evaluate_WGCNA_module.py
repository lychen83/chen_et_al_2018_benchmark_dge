"""This script is used to compare WGCNA module from reference transcriptome and Trinity cluster transcriptome"""

import sys,os

#for DNA of the same species:
SIMILARITY_CUTOFF = 0.98 #only consider HSPs >= this percentage similarity
MATCH_CUTOFF = 200 #only consider HSPs with # matches >= this

#for amino acids between closely-related species:
#SIMILARITY_CUTOFF = 0.9 #only consider HSPs >= this percentage similarity
#MATCH_CUTOFF = 50 #only consider HSPs with # matches >= this

def process_block(block):
	"""take a block of all blat output lines for one query and check"""
	if len(block) == 0:
		pass #no line passed the filters. Ignore the query
	elif len(block) == 1:
		best_hit = block[0]
		return best_hit
	else: #multiple blat his
		best_hit = block[0] #The first hit is the best hit. NOT always!!!! see following
		for hit in block[1:]: #find the best hit with max matching nucleotides
			if hit[0] > best_hit[0]: best_hit = hit
			if hit[0] == best_hit[0] and hit[17] <best_hit[17]: # The hit with less count is the best
				best_hit =hit
		return best_hit

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: DEG_plot_ROC.py blat_output_file working_dir name_of_interested_trinity_module_color name_of_interested_reference_module_color. If you have no idea about the interested module to compare, just input any nmae of de novo and reference module"
		sys.exit(0)
	blat_file = open(sys.argv[1],'r') # query is de novo transcripts, target is reference genes
	DIR = sys.argv[2]
	trinity_module_color = sys.argv[3]
	reference_module_color = sys.argv[4]
	
	
# 1 Get best matched gene for each supertranscript, longest transcript
	print "process file "+sys.argv[1]
	blat_output_lines = blat_file.readlines()
	if 'psLayout version 3' in blat_output_lines[0]: # ignor the title of the blat results
		blat_output_lines = blat_output_lines[5:]
	block = [] #all the hits (reference) within belong to one query (de novo)
	query_gene_dict = {} # key is transcript, value is the best matched gene
	last_query = ""
	all_gene_queries_dict = {} #key is reference gene, value is queries
	for line in blat_output_lines:	
		#Parse data
		if len(line) < 3: continue #ignore empty lines
		spls = line.strip().split('\t')
		if 'GSVIVT' in spls[13]:
			spls[13] = spls[13].replace('GSVIVT','GSVIVG')
		for column in [0,1,10,11,12,14,15,16]:
			spls[column] = float(spls[column]) #fix data type
		similarity = spls[0] / (spls[1]+spls[0])  #match/(match+mismatch)
		match = spls[0]
		query = spls[9]
		
		#Get blocks with the same query
		if similarity >= SIMILARITY_CUTOFF and match >= MATCH_CUTOFF:
			if query == last_query or block == []:
				block.append(spls)#add to block	
			else:   # new query
				best_hit_2 = process_block(block)#send the whole block off  Chen: whole block from last query--target
				query_gene_dict[best_hit_2[9]] = best_hit_2[13]
				if best_hit_2[13] not in all_gene_queries_dict:
					all_gene_queries_dict[best_hit_2[13]] = [best_hit_2[9]]
				else:
					all_gene_queries_dict[best_hit_2[13]].append(best_hit_2[9])
				block = [spls]#reset and initiate the block.  Chen: set this new query--target as block
			last_query = query
	best_hit_2 = process_block(block)#process the last block
	query_gene_dict[best_hit_2[9]] = best_hit_2[13]
	
	############################################################################
	################## Get the module information###############################
	############################################################################
	print 'processing the module information'
	ref_moduleid_gene_dict = {}  # key is the reference module ID, gene is reference gene id
	trinity_moduleid_gene_dict = {}   # key is the trinity module ID, gene is trinity transcript id
	for file in os.listdir(DIR):
		if file.startswith("trinity_kME"):
			file_lines = open(file,'r').readlines()
			for line in file_lines:
				line = line.strip()
				if len(line) < 5: continue #ignore empty lines
				ID = line.split(' ')[1].strip('"')
				if file not in trinity_moduleid_gene_dict:
					trinity_moduleid_gene_dict[file] = [ID]
				else: trinity_moduleid_gene_dict[file].append(ID)
		if file.startswith("reference_kME"):
			file_lines = open(file,'r').readlines()
			for line in file_lines:
				line = line.strip()
				if len(line) < 5: continue #ignore empty lines
				ID = line.split(' ')[1].strip('"')
				if file not in ref_moduleid_gene_dict:
					ref_moduleid_gene_dict[file] = [ID]
				else: ref_moduleid_gene_dict[file].append(ID)
	print 'trinity module number is', len(trinity_moduleid_gene_dict.keys())
	print 'reference module number is', len(ref_moduleid_gene_dict.keys())
	
	# calculate number of gene in trinity module
	num_trinityid = 0
	for n in trinity_moduleid_gene_dict.values():
		for o in n:
			num_trinityid  +=1
	print 'num_module_trinityid', num_trinityid
	
	# change trinity id to corresponding reference id
	trinity_moduleid_corresreference_dict = {} # key is trinity id, value is reference id
	for a in trinity_moduleid_gene_dict:
		for b in trinity_moduleid_gene_dict[a]:
			if b in query_gene_dict:
				if a not in trinity_moduleid_corresreference_dict:
					trinity_moduleid_corresreference_dict[a] = [query_gene_dict[b]]
				else: trinity_moduleid_corresreference_dict[a].append(query_gene_dict[b])
	# calculate number of gene in trinity module when change trinity id to reference id
	module_corresreference_list = []
	for n in trinity_moduleid_corresreference_dict.values():
		for o in n:
			module_corresreference_list.append(o)
	print 'num_module_corresreference', len(module_corresreference_list)
	print 'num_module_corresreference,removed duplicated', len(set(module_corresreference_list))
	
	#############################################################################################
	############construct connection between trinity module and reference module#################
	#############################################################################################
	name = os.path.basename(sys.argv[1])
	print ''
	print 'compare each trinity module to reference (trinity module used as query)'
	print 'ratio, shared genes, hub genes in trinity module (remove duplication), total hub genes in trinity module ---- only hub genes which can correspond to reference genes'
	with open(name + "WGCNA_evaluate_results.txt","a") as outfile:
		outfile.write('compare trinity module to reference (trinity module used as query)\n')
		outfile.write('ratio, shared genes, total hub genes in reference module\n')
	for t_module in trinity_moduleid_corresreference_dict:
		max_len = 0
		for r_module in ref_moduleid_gene_dict:
			num = len(set(trinity_moduleid_corresreference_dict[t_module]) & set(ref_moduleid_gene_dict[r_module]))
			if num > max_len: 
				max_len = num
		for r_module in ref_moduleid_gene_dict:
			num = len(set(trinity_moduleid_corresreference_dict[t_module]) & set(ref_moduleid_gene_dict[r_module]))
			if num == max_len:
				if float(num)/len(set(trinity_moduleid_corresreference_dict[t_module])) >= 0.3: 
					print t_module,r_module
					print str(round(float(num)/len(set(trinity_moduleid_corresreference_dict[t_module])),3)), num, len(set(trinity_moduleid_corresreference_dict[t_module])),len(trinity_moduleid_corresreference_dict[t_module])
					with open(name + "WGCNA_evaluate_results.txt","a") as outfile:
						outfile.write(str(t_module) + '\t' + str(r_module) + '\t' + '\n')
						outfile.write(str(round(float(num)/len(ref_moduleid_gene_dict[r_module]),3)) + '\t' + str(num) + '\t' + str(len(ref_moduleid_gene_dict[r_module])) + '\n')
				break

	print ''
	print 'compare reference module to trinity'
	print 'ratio, shared genes, total hub genes in reference module (reference module used as query)'
	with open(name + "WGCNA_evaluate_results.txt","a") as outfile:
		outfile.write('compare reference module to trinity (reference module used as query)\n')
		outfile.write('ratio, shared genes, total hub genes in reference module\n')
	for r_module in ref_moduleid_gene_dict:
		max_len = 0
		for t_module in trinity_moduleid_corresreference_dict:
			num = len(set(trinity_moduleid_corresreference_dict[t_module]) & set(ref_moduleid_gene_dict[r_module]))
			if num > max_len: max_len = num
		for t_module in trinity_moduleid_corresreference_dict:
			num = len(set(trinity_moduleid_corresreference_dict[t_module]) & set(ref_moduleid_gene_dict[r_module]))
			if num == max_len:
				if float(num)/len(ref_moduleid_gene_dict[r_module]) >= 0.3: 
					print r_module, t_module
					print str(round(float(num)/len(ref_moduleid_gene_dict[r_module]),3)), num, len(ref_moduleid_gene_dict[r_module])
					with open(name + "WGCNA_evaluate_results.txt","a") as outfile:
						outfile.write(str(t_module) + '\t' + str(r_module) + '\t' + '\n')
						outfile.write(str(round(float(num)/len(ref_moduleid_gene_dict[r_module]),3)) + '\t' + str(num) + '\t' + str(len(ref_moduleid_gene_dict[r_module])) + '\n')
				break
	#print set(trinity_moduleid_corresreference_dict['trinity_kMEturquoise'])
	print ''
	#print set(ref_moduleid_gene_dict['reference_kMEturquoise'])
	
	
	print 'compare the interested trinity module to all reference modules'
	print 'share number of genes, and proportion to number of genes in reference module'
	for r_module in ref_moduleid_gene_dict:
		num_transcripts = 0
		num = len(set(trinity_moduleid_corresreference_dict[trinity_module_color]) & set(ref_moduleid_gene_dict[r_module]))
		list = set(trinity_moduleid_corresreference_dict[trinity_module_color]) & set(ref_moduleid_gene_dict[r_module])
		for a in trinity_moduleid_gene_dict[trinity_module_color]:
			if a in query_gene_dict:
				if query_gene_dict[a] in list:
					
					num_transcripts += 1
				
		proportion =  round(float(num)/len(set(ref_moduleid_gene_dict[r_module])), 3)
		print trinity_module_color, r_module
		print num, proportion, num_transcripts 
		
	print ''
	print 'compare the interested reference module to all trinity modules'
	print 'share number of genes, proportion of number of genes in trinity module, number of genes in trinity module (remove duplication), number of genes in trinity_module (no remove duplication)'
	for t_module in trinity_moduleid_corresreference_dict:
		num = len(set(ref_moduleid_gene_dict[reference_module_color]) & set(trinity_moduleid_corresreference_dict[t_module]))
		proportion = round(float(num)/len(set(trinity_moduleid_corresreference_dict[t_module])), 3)
		print reference_module_color, t_module
		print num, proportion, len(set(trinity_moduleid_corresreference_dict[t_module])), len(trinity_moduleid_corresreference_dict[t_module])
	