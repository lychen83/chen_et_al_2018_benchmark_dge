"""get the longest isoform of reference annotation"""

import sys,os

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: get_longest_reference.py reference_transcripts"
		sys.exit(0)
	fasta_file = open(sys.argv[1],'r').readlines() # fasta file
	print "process file "+sys.argv[1]
	cluster_dict = {}
	id_len_dict = {}
	seq = ''
	for line in fasta_file:
		if len(line) < 3: continue #ignore empty lines
		if '>' in line:
			length = 0
			id = line.strip().split(' ')[0]
			if '.' in id:
				geneid = line.strip().split(' ')[0].split('.')[0]
				transcriptid = line.strip().split(' ')[0]
				if geneid not in cluster_dict:
					cluster_dict[geneid] = [line.strip().split(' ')[0]]
				else:
					cluster_dict[geneid].append(line.strip().split(' ')[0])
			if '_T' in id:  # Some reference isoform was marked using _T.
				geneid = line.strip().split(' ')[0].split('_T')[0]
				transcriptid = line.strip().split(' ')[0]
				if geneid not in cluster_dict:
					cluster_dict[geneid] = [line.strip().split(' ')[0]]
				else:
					cluster_dict[geneid].append(line.strip().split(' ')[0])
			if '.' not in id and '_T' not in id: #Some reference isoform was marked using .1, .2....
				geneid = line.strip().split(' ')[0]
				cluster_dict[geneid] = geneid
				transcriptid = line.strip().split(' ')[0]
		
		else:
			length = len(line.strip()) + length
			id_len_dict[transcriptid] = length
	# print id_len_dict
	name = os.path.basename(sys.argv[1])

	extracted_id = []
	for a in cluster_dict:
		if len(cluster_dict[a])  == 1:
			for zz in cluster_dict[a]:
				extracted_id.append(str(zz))
		if len(cluster_dict[a])  > 1:
			max_len = 0
			for b in cluster_dict[a]:
				#print b
				#print float(id_len_dict[b]), max_len
				if float(id_len_dict[b]) > max_len:                             
					max_len = id_len_dict[b]
			for x in cluster_dict[a]:
				if float(id_len_dict[x]) == max_len:
					extracted_id.append(str(x))
					break
		if len(cluster_dict[a]) == 0:
			print 'error may happen'
	print 'in total, clusters is', len(cluster_dict), len(set(cluster_dict))
	print 'in total, extracted sequences is', len(set(extracted_id))
	# print extracted_id
	print 'writing extracted sequences'
	num_seq_writing = 0
	with open(name + "longest.fa","a") as outfile:
		for line in fasta_file:
			if len(line) < 2: continue #ignore empty lines
			if '>' in line and line.strip().split(' ')[0] in extracted_id:
				label = 'True'
				outfile.write(line.strip().split(' ')[0] + '\n')
				num_seq_writing += 1
			if '>' in line and line.strip().split(' ')[0] not in extracted_id:
				label = 'False'
			else:
				if '>' not in line and label == 'True':
					outfile.write(line)
	print 'wrote sequence number is', num_seq_writing