"""This script is to compare the deseq2 results from reference and de novo transcript"""

import sys,os

#for DNA of the same species:
SIMILARITY_CUTOFF = 0.98 #only consider HSPs >= this percentage similarity
MATCH_CUTOFF = 100 #only consider HSPs with # matches >= this


def process_cluster(cluster_infor):
	# Using the cluster information, get correspondence between cluster and gene
	print 'proces cluster information'
	global query_gene_dict, query_gene_blatscore_dict
	cluster_infor = cluster_infor.readlines()
	cluster_dict = {} # such as {'cluster1':['Tran1','Trans2'],'cluster2:'....}
	for line in cluster_infor:
		if len(line) < 3: continue #ignore empty lines
		spls = line.strip().split('\t')
		if spls[1] not in cluster_dict: 
			cluster_dict[spls[1]] = [spls[0]]
		else: cluster_dict[spls[1]].append(spls[0]) # such as {'cluster1':['Tran1','Trans2'],'cluster2:'....}

	cluster_gene_dict = {}
	for i in cluster_dict:
		count_gene_present = {} # {'gene1':2, 'gene2':3,}
		gene_query_dict = {} # key is gene, values is its correspond query within the cluster
		for a in cluster_dict[i]: # a is such as 'Tran1'.
			if a in query_gene_dict:
				if query_gene_dict[a] not in count_gene_present:
					count_gene_present[query_gene_dict[a]] = 1 
				else: count_gene_present[query_gene_dict[a]] += 1 
				
				if query_gene_dict[a] not in gene_query_dict:
					gene_query_dict[query_gene_dict[a]] = [a]
				else: gene_query_dict[query_gene_dict[a]].append(a)  # Get {'gene1':['Trans1','Trans2'], 'gene2':..}
		if len(count_gene_present) == 0: continue
		max_num = max(count_gene_present.values())
		majority_genes_in_cluster = [] # such as ['gene1,', 'gene2']
		for c in count_gene_present:
			if count_gene_present[c] == max_num:
				majority_genes_in_cluster.append(c)
				
		# add cluster to gene relationship to cluster_gene_dict
		if len(majority_genes_in_cluster) == 1: # only include one gene
			cluster_gene_dict[i] = ''.join(majority_genes_in_cluster) # convert list to string
		else: # cluster include multiple genes, get the gene with longest match length to query
			max_len = 0
			for a in majority_genes_in_cluster: 
				for query in gene_query_dict[a]:
					query = ''.join(query)
				if query_gene_blatscore_dict[query] > max_len:
					max_len = query_gene_blatscore_dict[query]
					gene = a
			cluster_gene_dict[i] = gene
	return cluster_gene_dict

def process_block(block):
	"""take a block of all blat output lines for one query and check"""
	if len(block) == 0:
		pass #no line passed the filters. Ignore the query
	elif len(block) == 1:
		best_hit = block[0]
		return best_hit
	else: #multiple blat his
		best_hit = block[0] #The first hit is the best hit. NOT always!!!! see following
		for hit in block[1:]: #find the best hit with max blat score
			if hit[11] > best_hit[11]: best_hit = hit
		return best_hit

if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "usage: evaluating_degs_longest_orf.py blat_output_file DESeq2_ref DESeq2_denovo chimera_id cluster_infor"
		sys.exit(0)
	blat_file = open(sys.argv[1],'r') # query is de novo transcripts, target is reference genes
	DESeq2_ref = open(sys.argv[2], 'r') # result from DESeq2
	DESeq2_denovo = open(sys.argv[3], 'r')
	chimera_file = open(sys.argv[4],'r')
	cluster_infor = open(sys.argv[5], 'r') # cluster infor from corset, rapclust. Such as'TRINITY_DN69886_c0_g1_i1	Cluster-0.0'
	name = os.path.basename(sys.argv[3])
# 1 Get best matched gene for each supertranscript, longest transcript
	print "process file "+sys.argv[1]
	blat_output_lines = blat_file.readlines()
	if 'psLayout version 3' in blat_output_lines[0]: # ignor the title of the blat results
		blat_output_lines = blat_output_lines[5:]
	block = [] #all the hits (reference) within belong to one query (de novo)
	query_gene_dict = {} # key is transcript, value is the best matched gene
	query_gene_blatscore_dict = {} # key are transcript, value is the blatscore
	last_query = ""
	chimera_file = chimera_file.readlines()
	chimera_id = []
	for chimera in chimera_file:
		chimera_id.append(chimera.strip())
	for line in blat_output_lines:	
		#Parse data
		if len(line) < 3: continue #ignore empty lines
		spls = line.strip().split('\t')
		if '.' in spls[1]:  # some reference annotation such as arabidopsis include multiple isoforms (AT5G67610.2, AT5G67610.1). Ignore the .1, .2..
			spls[1] = str(spls[1].split('.')[0])
		if spls[0] in chimera_id: continue
		for column in [2,3,4,5,6,7,8,9,11]:
			spls[column] = float(spls[column]) #fix data type
		similarity = spls[2] 
		match = spls[3]
		query = spls[0]
		
		#Get blocks with the same query
		if similarity >= SIMILARITY_CUTOFF and match >= MATCH_CUTOFF:
			if query == last_query or block == []:
				block.append(spls)#add to block	
			else:   # new query
				best_hit_2 = process_block(block)#send the whole block off  Chen: whole block from last query--target
				query_gene_dict[best_hit_2[0]] = best_hit_2[1]
				query_gene_blatscore_dict[best_hit_2[0]] = best_hit_2[11]
				block = [spls]#reset and initiate the block.  Chen: set this new query--target as block
			last_query = query
	best_hit_2 = process_block(block)#process the last block
	query_gene_dict[best_hit_2[0]] = best_hit_2[1]
	query_gene_dict = process_cluster(cluster_infor)

# 2 sort the DESeq2_denovo with p <= 0.05 and |log2fold| >=2 by |log2fold|
	print 'sortting the DESeq results by pvalues'
	lines_DESeq2_denovo = 0 #DESeq results based on de novo results include lines (without NA)
	denovo_DEG_list = [] # de novo results with p values <=0.05
	DESeq2_denovo = DESeq2_denovo.readlines()
	denovo_query_fold2change_dict = {}
	num_denovo_DEG = 0
	query_denovo_line_dict = {} # key is query id, value is the line from deseq2
	if 'baseMean' in DESeq2_denovo[0]: # ignor the title of the blat results
		DESeq2_denovo = DESeq2_denovo[1:]
	denovo_DEG_log2fold_dict= {} # key is line, value is log2fold
	positively_list = []
	for i in DESeq2_denovo:
		if len(line) < 2: continue #ignore the empty lines
		i = i.strip()
		i = i.split(',')
		if i[1] == "NA" or i[2] == "NA" or i[5] == "NA" or i[6] == "NA": continue # ignore the line with 'log2FoldChang'e is 'NA', 'pvalue' is 'NA'
		lines_DESeq2_denovo += 1
		i[0] = i[0].replace('\"','') # delete the " symbol in cluster ID such as. Otherwise cannot work
		denovo_query_fold2change_dict[i[0]] = float(i[2]) # key is de novo query id, value is fold2change
		if float(i[1]) >=20 and abs(float(i[2])) >= 2 and float(i[5]) <= 0.05 and float(i[6]) <= 0.05:
			denovo_DEG_list.append(i[0])
			num_denovo_DEG += 1
			query_denovo_line_dict[i[0]] = i
			denovo_DEG_log2fold_dict[i[0]] = abs(float(i[5]))
			positively_list.append(i)
			
	for abcd in positively_list:
		with open(name +"_positively_selected_list","a") as outfile:
			outfile.write(str(",".join(abcd)) +"\n")
		denovo_DEG_log2fold_dict_ranked = sorted(denovo_DEG_log2fold_dict.items(), key=lambda item: item[1],reverse = True) # ranked by log2fold 
	denovo_DEG_list = []
	denovo_DEG_log2fold_dict_ranked = str(denovo_DEG_log2fold_dict_ranked).split('\'')
	for tt in denovo_DEG_log2fold_dict_ranked:
		if 'TRINITY' in tt or 'Cluster' in tt or 'cluster' in tt:
			denovo_DEG_list.append(tt)
	percent30_denovo_DEG_list = denovo_DEG_list[0:len(denovo_DEG_list)*3/10]

# 4 get the total positive and total negative from DESeq2_ref
	print 'getting DESeq from reference'
	num_total_up_positive = 0 #only the number, which matched between de novo and reference
	total_up_positive = []
	num_total_down_positive = 0 #only the number, which matched between de novo and reference
	total_down_positive = []
	num_total_negative = 0 
	total_positive = []
	total_negative = []
	num_reference_DEG = 0
	num_total_positive = 0
	lines_DESeq2_ref = 0
	DESeq2_ref = DESeq2_ref.readlines()
	if 'log2FoldChange' in DESeq2_ref[0]:
		DESeq2_ref = DESeq2_ref[1:]
	ref_query_fold2change_dict = {} # key is de novo query id, value is fold2change
	ref_DEG_log2fold_dict = {}
	for i in DESeq2_ref:
		if len(line) < 2: continue #ignore the empty lines
		i = i.strip()
		i = i.split(',')
		if i[2] == "NA" or i[5] == "NA" or i[6] == "NA": continue # ignore the line with 'log2FoldChang'e is 'NA', 'pvalue' is 'NA'
		lines_DESeq2_ref += 1
		i[0] = i[0].replace('\"','')
		i[0] = i[0].split('.')[0]
		if float(i[1]) >=20 and float(i[2]) >= 2 and float(i[5]) <= 0.05 and float(i[6]) <= 0.05:
			num_total_up_positive +=1
			total_up_positive.append(i[0])
		if float(i[1]) >=20 and float(i[2]) <= -2 and float(i[5]) <= 0.05 and float(i[6]) <= 0.05:
			num_total_down_positive +=1
			total_down_positive.append(i[0])
		if float(i[1]) <20 or (abs(float(i[2])) < 2 or float(i[5]) > 0.05) or float(i[6]) > 0.05:
			num_total_negative += 1
			total_negative.append(i[0])
		total_positive = total_up_positive + total_down_positive
	num_total_positive = len(total_positive)
	num_reference_DEG = num_total_positive

	print 'DESeq results based on de novo results include lines (without NA)', lines_DESeq2_denovo
	print 'DESeq results based on reference include lines (without NA)', lines_DESeq2_ref
	print 'num_reference_DEG is', num_reference_DEG
	print 'num_denovo_DEG is', num_denovo_DEG
	print 'number of total reference positive, which have matches in denovo', num_total_positive
	print 'number of total reference negatives, which have matches in denovo', num_total_negative
	print 'number of total up regulated reference positives, which have matches in denovo', num_total_up_positive
	print 'number of total down regulated reference positives, which have matches in denovo', num_total_down_positive
	print 'number of 30 percent top log2fold ranked deDEG', len(percent30_denovo_DEG_list)

#5.1 analysis the composition of the denovo DEG. The purpose is to get the transcripts which cannot matches to any reference genes
	num_true_positive = 0
	num_false_positive = 0
	unmatched_denovo_DEG = []
	num_NA_denovo = 0
	num_denovo_deg_match_ref = 0
	num_contradict_direction = 0
	#print query_gene_dict
	for DEG in percent30_denovo_DEG_list:
		if DEG in query_gene_dict:
			num_denovo_deg_match_ref +=1
			if float(query_denovo_line_dict[DEG][2]) >= 2 and query_gene_dict[DEG] in total_up_positive:
				num_true_positive += 1
				#print 'xxxxxx'
			if float(query_denovo_line_dict[DEG][2]) <= -2 and query_gene_dict[DEG] in total_down_positive:
				num_true_positive += 1 
				#print 'yyyyyy'
			if float(query_denovo_line_dict[DEG][2]) <= -2 and query_gene_dict[DEG] in total_up_positive:
				num_false_positive += 1
				num_contradict_direction +=1
				#print 'zzzzzz'
			if float(query_denovo_line_dict[DEG][2]) >= 2 and query_gene_dict[DEG] in total_down_positive:
				num_false_positive += 1
				num_contradict_direction +=1
				#print 'aaaaa'
			if query_gene_dict[DEG] in total_negative:
				num_false_positive += 1
				#print 'bbbbb'
			if query_gene_dict[DEG] not in total_positive and query_gene_dict[DEG] not in total_negative:
				num_NA_denovo +=1
		else: 
			unmatched_denovo_DEG.append(DEG)
	
		
	denovo_DEG_gene_list = []
	for zz in denovo_DEG_list:
		if zz in query_gene_dict:
			denovo_DEG_gene_list.append(query_gene_dict[zz])
	num_sensitivity = 0
	for DEG in total_positive:
		if DEG in denovo_DEG_gene_list:
			num_sensitivity += 1
	
	print 'num_denovo_DEG_not_in_correspondence', len(unmatched_denovo_DEG)
	if os.path.exists(name +"_unmatched_denovo_DEG"):
		os.remove(name +"_unmatched_denovo_DEG")
	with open(name +"_unmatched_denovo_DEG","a") as outfile:
		for x in unmatched_denovo_DEG:
			outfile.write(str(x) +"\n")
	print 'num_true_positive', num_true_positive
	print 'num_false_positive', num_false_positive
	print 'num_denovo_deg_match_ref', num_denovo_deg_match_ref
	print 'num_contradict_direction', num_contradict_direction
	print 'num de novo DEG matched to reference genes with DESeq2 results NA', num_NA_denovo

# 5 calculate TPR and FPR,F score, etc.
	TPR = float(num_true_positive) / (num_true_positive + num_false_positive)
	FPR = float(num_false_positive) / (num_true_positive + num_false_positive)
	sensitivity = float(num_sensitivity) / (num_total_positive + 0.01) # 0.01 to avoid the num_total_positive is 0
	print "True positive rate is", round(TPR,3)
	print "False positive rate is", round(FPR,3)
	print 'sensitivity is', round(sensitivity,3)
	print ''
	
	with open(name +"_DGE_evaluate_result","a") as outfile:
		outfile.write("num_reference_DEG is " + str(num_reference_DEG) +"\n")
		outfile.write("num_denovo_DEG is " + str(num_denovo_DEG) +"\n")
		outfile.write("number of 30 percent top log2fold ranked deDEG is " + str(len(percent30_denovo_DEG_list)) +"\n")
		outfile.write("num_true_positive " + str(num_true_positive) +"\n")
		outfile.write("num_false_positive " + str(num_false_positive) +"\n")
		outfile.write("precision is" + str(float(num_true_positive)/(num_true_positive + num_false_positive)) + "\n")
		outfile.write("num de novo DEG matched to reference genes with DESeq2 results NA " + str(num_NA_denovo) +"\n")
		outfile.write("num_denovo_DEG_not_in_correspondence " + str(len(unmatched_denovo_DEG)) +"\n")
		outfile.write("sensitivity(coverage) is " + str(round(sensitivity,3)) +"\n")