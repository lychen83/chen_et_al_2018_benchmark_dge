"""This script is used to extract the longest contig within a cluster"""

import sys, os, string
from seq import read_fasta_file

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: get_longest_isoform_cluster.py fasta_file cluster_file"
		sys.exit(0)
	fasta_file = sys.argv[1] # sequence file, such as the fasta from Trinity
	cluster_file = open(sys.argv[2], 'r').readlines() # first column of cluster file is transcript ID, second is cluster ID
	
	# processing the fasta file
	len_dict = {} # key is contig ID, value is length
	seq_dict = {} # key is contig ID, value is sequence
	print 'getting length of fasta file'
	symbol = ''
	seqlist = read_fasta_file(fasta_file)
	for s in seqlist:
		len_dict[s.name.split(' ')[0].split('.')[0]] = len(s.seq.strip())
		seq_dict[s.name.split(' ')[0].split('.')[0]] = s.seq
	
	# processing the cluster file
	cluster_dict = {} #key is cluster ID, value is contig ID
	print 'processing the cluster file'
	for line in cluster_file:	
		if len(line) < 3: continue #ignore empty lines
		cluster_id = ''.join(line.split('\t')[1].split())
		if cluster_id not in cluster_dict:
			cluster_dict[cluster_id] = [line.split('\t')[0]]
		if cluster_id in cluster_dict:
			cluster_dict[cluster_id].append(line.split('\t')[0])
	print 'number of cluster is', len(cluster_dict)
	#print cluster_dict
	# Getting the longest contig within a cluster
	print 'getting the longest contig ID'
	longest_contig_id = []
	for a in cluster_dict:
		max_len = 0
		for b in cluster_dict[a]:
			if b in len_dict:
				#print b
				if len_dict[b] > max_len:
					max_len = len_dict[b]
		for b in cluster_dict[a]:
			if b in len_dict:
				if len_dict[b] == max_len:
					longest_contig_id.append(b)
					break
		
	# Output the longest contig ID
	print 'num of longest contig is', len(set(longest_contig_id))
	print 'writing the longest contigs'
	name = os.path.basename(sys.argv[2]).replace('.txt','')
	num = 0
	with open(name + ".longest_orf.fasta","a") as outfile:
		for a in longest_contig_id:
			if a in seq_dict:
				outfile.write('>' + str(a) + '\n' + str(seq_dict[a]) + '\n')
				num += 1
	print 'write', num, 'longest contigs'