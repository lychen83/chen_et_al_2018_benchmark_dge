Most analyses were conducted at the Mesabi compute cluster in Minnesota Supercomputing Institute (https://www.msi.umn.edu/content/mesabi). A few analyses were conducted using a workstation with 24 cores and 96 Gb RAM in Ya Yang’s lab, University of Minnesota Twin Cities. 

Data for the five species will be deposited in Dryad when the peer review completed. 
Example commands were provided in the following instruction:

###Step 1: RNA-seq reads processing and de novo transcriptome assembling
1.1 Convert dataset from sra format to fastq format using fastq-dump, and correct sequencing errors using Rcorrector
```
<path_to_fastq-dump_folder/fastq-dump> --defline-seq '@$sn[_$rn]/$ri' --split-files <SRRXXXXXX.sra>; perl <path_to_rcorrector_folder/run_rcorrector.pl> -t 5 -p <SRRXXXXXX_1.fastq> <SRRXXXXXX_2.fastq>
```

1.2 Filter adapter and low quality bases using Trimmomatic
Adapter sequences were accessed from https://support.illumina.com/content/dam/illumina-support/documents/documentation/chemistry_documentation/experiment-design/illumina-adapter-sequences-1000000002694-10.pdf
An example for paired-end reads
```
 java -jar <path_to_trimmomatic_folder/trimmomatic-0.36.jar> PE -threads 10 -phred33 <SRRXXXXXX_1.cor.fq> <SRRXXXXXX_2.cor.fq> <SRRXXXXXX_1.cor.p.fq> <SRRXXXXXX_1.cor.u.fq> <SRRXXXXXX_2.cor.p.fq> <SRRXXXXXX_2.cor.u.fq> ILLUMINACLIP:<path_to_adapter_folder/TruSeq_adapters>:2:30:10 SLIDINGWINDOW:4:15 LEADING:5 TRAILING:4 MINLEN:80
```
For single end reads, 'SE' option was used 
In order to save storage space, fastq files can be compressed to gz format, which is readable for Trinity and Salmon
```
gzip <SRRXXXXXX_1.cor.p.fq>

1.3 Confirm that adapters have been removed.
Adapter sequences were first searched against reads using BLASTn to confirm that adapters have been removed; reads were also searched against the adapters. The adapter sequences and examples of the BLASTn results can be accessed from https://bitbucket.org/lychen83/chen_et_al_2018_benchmark_dge/src/master/1.3_BLASTn_BLAT_confirm_adapters_trimmed/
```

1.4 De novo transcriptome assembling using Trinity
```
<path_to_trinity_folder/Trinity> --seqType fq --max_memory 90G  --output arabidopsis_trinityv2.6.6 --samples_file arabidopsis_trinity_sample.txt --CPU 10
```
The 'arabidopsis_trinity_sample.txt' includes sample information as following:
```
cond_A  cond_A_rep1         <path_to_reads_folder/SRR3371309_1.cor.p.fq.gz>
cond_A  cond_A_rep2         <path_to_reads_folder/SRR3371320_1.cor.p.fq.gz>
cond_A  cond_A_rep3         <path_to_reads_folder/SRR3371331_1.cor.p.fq.gz>
cond_B  cond_B_rep1         <path_to_reads_folder/SRR3371299_1.cor.p.fq.gz>
cond_B  cond_B_rep2         <path_to_reads_folder/SRR3371296_1.cor.p.fq.gz>
cond_B  cond_B_rep3         <path_to_reads_folder/SRR3371297_1.cor.p.fq.gz>
cond_C  cond_C_rep1         <path_to_reads_folder/SRR3371313_1.cor.p.fq.gz>
cond_C  cond_C_rep2         <path_to_reads_folder/SRR3371314_1.cor.p.fq.gz>
cond_C  cond_C_rep3         <path_to_reads_folder/SRR3371315_1.cor.p.fq.gz>
cond_D  cond_D_rep1         <path_to_reads_folder/SRR3371356_1.cor.p.fq.gz>
cond_D  cond_D_rep2         <path_to_reads_folder/SRR3371354_1.cor.p.fq.gz>
cond_D  cond_D_rep3         <path_to_reads_folder/SRR3371342_1.cor.p.fq.gz>
cond_E  cond_E_rep1         <path_to_reads_folder/SRR3371300_1.cor.p.fq.gz>
cond_E  cond_E_rep2         <path_to_reads_folder/SRR3371301_1.cor.p.fq.gz>
cond_E  cond_E_rep3         <path_to_reads_folder/SRR3371302_1.cor.p.fq.gz>
cond_F  cond_F_rep1         <path_to_reads_folder/SRR3371316_1.cor.p.fq.gz>
cond_F  cond_F_rep2         <path_to_reads_folder/SRR3371317_1.cor.p.fq.gz>
cond_F  cond_F_rep3         <path_to_reads_folder/SRR3371318_1.cor.p.fq.gz>
cond_G  cond_G_rep1         <path_to_reads_folder/SRR3371335_1.cor.p.fq.gz>
cond_G  cond_G_rep2         <path_to_reads_folder/SRR3371336_1.cor.p.fq.gz>
cond_G  cond_G_rep3         <path_to_reads_folder/SRR3371337_1.cor.p.fq.gz>
```

1.5 Using rnaQUAST to get number of covered reference bases and IDs of the misassembled transcripts for a de novo transcriptome
```
python <path_to_rnaQUAST-1.5.0_folder/rnaQUAST.py> --transcripts <de_novo_transcriptome> --reference <genome_DNA> --gtf <genome_annotation.gff3> -t 16 --blat -o <output_folder_name>
``` 

1.6 Estimate the proportion of expressed reference bases
Combine reads from different samples of a species to a single file. 
```
cat <all left reads> >all_left.fq
cat <all right reads> >all_right.fq
```
Index the reference genes
```
bowtie2-build <input_reference_genes> <output_file_bowtie_index>
```
Mapping reads to reference genes
```
bowtie2 -p 8  -x <output_file_bowtie_index> -1 <pair1.fq> -2 <pair2.fq> | samtools view -bS - > <bowtie2_mapping.bam>
```
Sort the bam file
```
samtools sort --threads 6 -m 6000m <bowtie2_mapping.bam> > <bowtie2_mapping.sorted.> --output-fmt bam
```
Estimate the depth for each base
```
samtools <bowtie2_mapping.sorted.bam> > <depth.txt>
```
Estimate the number of expressed reference bases. This command will report the total number of reference bases, the number and proportion of expressed reference bases (under a threshold)
```
python calculate_bam_reference_coverage.py <depth.txt>
```
Last, coverage of the expressed reference bases was calculated by the number of covered reference bases divided by the total number of expressed reference bases.

###Step 2: Obtain cluster information
2.1 Trinity cluster
The cluster information was extracted from the transcript IDs
```
python extract_Trinity_cluster.py arabidopsis_coolen_2016_Trinity2.6.6.fasta
```
This command generated a cluster file. The file includes two columns. The first column is transcript ID, the second is cluster ID

2.2 CD-HIT-EST cluster
Before running the CD-HIT-EST, removing the characters 'TRINITY_DN' from de novo transcript IDs. Otherwise, the transcript IDs in the cluster information generated by CD-HIT-EST may be incomplete
```
sed '/^>/ s/TRINITY_DN//' arabidopsis_coolen_2016_Trinity2.6.6.fasta >arabidopsis_coolen_2016_Trinity2.6.6_2.fasta
```
Run CD-HIT-EST：
```
cd-hit-est -i arabidopsis_coolen_2016_Trinity2.6.6_2.fasta -c 0.85 -o arabidopsis_coolen_2016_Trinity2.6.6_2_cdhit_0.85 -T 10 -M 5500
```

CD-HIT-EST generated two files. The file ended with '.clstr' contains cluster information. Convert the file to the same format of Trinity cluster file using the following command
```
python extract_cdhit_clusters.py <XXX.clstr(from_CD-HIT-EST)>
```

2.3. Corset cluster
Index Trinity assembly using software Salmon
```
salmon index -t arabidopsis_coolen_2016_Trinity2.6.6.fasta -i arabidopsis_coolen_2016_Trinity2.6.6.salmon_index
```

Map reads to Trinity assembly and get expression data
```
cat <file_include_all_SRA_ID> | while read f;  do salmon quant -p 4 -i arabidopsis_coolen_2016_Trinity2.6.6.salmon_index --libType IU --dumpEq -1 ${f}"_1.cor.p.fq.gz" -2 ${f}"_2.cor.p.fq.gz" --output ${f}"Trinity_assembly.salmon_map"; done
```

Run Corset (Arabidopsis data as an example)
```
corset -f true -g 1,1,1,2,2,2,5,3,3,3,4,4,4,5,5,6,6,6,7,7,7 -n SRR3371296,SRR3371297,SRR3371299,SRR3371300,SRR3371301,SRR3371302,SRR3371309,SRR3371313,SRR3371314,SRR3371315,SRR3371316,SRR3371317,SRR3371318,SRR3371320,SRR3371331,SRR3371335,SRR3371336,SRR3371337,SRR3371342,SRR3371354,SRR3371356 -i salmon_eq_classes <path_to_mapping_results_from_Salmon/*Trinity_assembly.salmon_map/aux_info/eq_classes.txt>
```
The cluster information was in the generated file ‘clusters.txt’.

2.4 Grouper cluster
Grouper used the Salmon mapping results from Step 2.3
```
Grouper --config arabidopsis_grouper_config.yaml
```
‘arabidopsis_grouper_config.yaml’ is the configure file.

Cluster information was in the generated file ‘mag.flat.clust’. The first column is cluster ID, second is transcript ID, which is opposite to Trinity cluster and Corset cluster, where first column is transcript ID, second is cluster ID. Change first column to second column, and second to first
```
awk 'BEGIN {OFS="\t"} {print $2, $1}' mag.flat.clust ><new_file_name>
```

###Step 3: Evaluate cluster quality
3.1 Search do novo transcripts against reference genes using BLAT
```
<path_to_pblat/pblat> Athaliana_167_TAIR10.transcript.fa arabidopsis_coolen_2016_Trinity2.6.6.fasta arabidopsis_coolen_2016_Trinity2.6.6.Athaliana_167_TAIR10.transcript_primaryTranscriptOnly_score.psl -threads=20 -out=blast8 
#Athaliana_167_TAIR10.transcript.fa is the reference genes. arabidopsis_coolen_2016_Trinity2.6.6.Athaliana_167_TAIR10.transcript_primaryTranscriptOnly_score.psl is the output file. 
```

3.2 Evaluating quality of clustering by pairwise combination of transcripts
```
python evaluate_clustering.py <blat_output_file> <de_novo_cluster_file> <chimera_id_file> <bitscore_ratio>
```
'blat_output_file', query are de novo transcripts, target are reference genes. In this study, for all the 'blat_output_file', query are de novo transcripts, target are reference genes
'chimera_id_file' file includes ids of the misassembled transcripts, which was generated by rnaQUAST
'bitscore_ratio' is the bit score ratio used to exclude the ambiguously assigned transcripts. For example, 0.98 means excluding transcripts with the bit score ratio >=0.98
This command generated parts of results in Figure 1 and Table S2. Figure 1 was plotted using Microsoft Excel 2010. 

3.3 Calculate proportion of ambiguously assigned transcript caused by paralogs
First, BLASTp reference genes within a reference genome against themself, and identify the possible homologous pairs.
```
python find_homologs_within_species.py <num_threads> <name_reference_gene_file>
```
The generated file end with 'filtered' includes all the possible homologous pairs
Next, identify the proportion of ambiguously assinged genes caused by homologs
```
python evaluate_ambigous_transcripts.py <blat_output_file> <*.filtered> 0.98
```

3.4 Plot the percent identity of the False Positives generated from evaluating clustering by pairwise combination of transcripts
```
python calculate_plot_identity_FP.py <blat_output_file> <file_include_false_positives> <min_match_length>
```
'file_include_false_positives', generated from step 3.3
'min_match_length', is the minimum match length for the two transcripts within a False Positive. For example, 100 means that the percent identity will be calculated for the two transcripts within the False Positive if the match length >=100. Otherwise, the percent identity will be treated as 0.
This command will generate a pdf file, which includes the distribution of percent identity

3.5 Study the homology issue and coverage issue for the False Nagetives in clustering
We evaluated all False Negatives by checking if the two transcripts within each negative had overlap (homologous issue) or not (coverage issue). Specifically, one transcript within a False Negative was searched against another with BLAT, if they have identity ≥90% and match length ≥50 bp, the False Negative was counted as caused by homologous issue, otherwise as caused by coverage issue. 
```
python calculate_homolog_coverage_isssue.py <blat_output_file> <list_FN> <min_match_length>
'min_match_length', is the minimum match length for the two transcripts within a false negative. For example, 50 means if the match length >=50, the False Negative will be treated as due to homolog issue
```

###Step 4: Differential gene expression (DGE) analyses
4.1 Differential gene expression analyses using longest ORFs within clusters
First, run software Transdecoder to predict ORF sequence for each transcript
```
TransDecoder.LongOrfs -t <de_novo_transcriptome> -m 50
```
```
TransDecoder.Predict -t <de_novo_transcriptome>
```

Transdecoder generated a file end with 'longest_orfs.cds', which is the nucleotide coding sequence for all detected ORFs. Extract the longest ORFs within clusters.
```
 python get_longest_isoform_cluster.py longest_orfs.cds <cluster_file>
```
Next, index and map reads to the transcriptomes composed of longest ORFs
```
salmon index -t <transcripome_composed_longest_ORF> -i <XXX.longest_orf.salmon_index>
```
Mapping
```
cat <file_include_all_SRA_ID> | while read f;  do salmon quant -p 4 -i <XXX.longest_orf.salmon_index> --libType IU --dumpEq -1 ${f}"_1.cor.p.fq.gz" -2 ${f}"_2.cor.p.fq.gz" --output ${f}"XXX.longest_contig.salmon_map"; done
```

Last, DGE analysis using software DESeq2. 
An example script for DGE analysis and the output from Salmon mapping were provided in the folder '4.1_dge_analysis_longest_ORF_within_clusters_deseq2'.
In addition, map reads to reference genome genes, and conduct DGE analyses. The results were used as reference to evaluate the performance of DGE analysis

4.2 DGE analyses using longest transcripts within clusters
First, extract longest transcripts within clusters using script 'get_longest_isoform_cluster.py'
```
python get_longest_isoform_cluster.py <transcriptome> <cluster_file>
```
Next, index the longest transcripts, mapping reads to transcripts, and run DESeq2 using a similar command in Section 4.1

4.3 DGE analyses by summarizing reads within clusters
First, extract transcript sequences. Cluster files from Trinity and CD-HIT-EST included all de novo transcripts. However, Corset and Grouper defaultly remove low expressed transcripts. Therefore, Corset and Grouper clusters may only include parts of the de novo transcripts. We extracted the transcripts included in Corset and Grouper clusters, then mapped reads to these transcripts
```
cut -f1 <XXX_clusters.txt> ><XXX_clusters_transcripts_id>         # Get transcripts IDs in a cluster file
```
```
perl extract_fasta_using_id.pl <transcript_id_file> <de_novo_transcriptome>
```

Next, conduct Salmon index and map reads to transcriptome using similar command lines as section 4.1

Last, DGE analyses using DESeq2. 
An example R script and the Salmon output were provided in folder '4.3_dge_analysis_summarize_transcripts_within_clusters_deseq2'.

###Step 5: Evaluating DGE analyses
5.1 and 5.2 Evaluating performance of DGE analyses using the longest ORFs, and using the longest transcripts separately
```
python Evaluating_DEGs_blat_score.py <blat_output_file> <DESeq2_result_using_reference> <DESeq2_result_using_longest_orfs_or_transcripts> <chimera_transcript_id> <bitscore_ratio>
```

5.3 Evaluating performance of DGE analyses by summarizing expression within a cluster
```
python Evaluating_DEGs_summarize_blat_score.py <blat_output_file> <DESeq2_result_using_reference> <DESeq2_result_summarizing_expression_within_cluster> <chimera_transcript_id> <cluster_file>
```
Step 5.1, 5.2 and 5.3 generated the results in Figure 2 and Supplementary Table S3. Figure 2 was plotted using Excel 2010.

###Step 6: Conduct gene co-expression analysis, and compare modules recovered from de novo assemblies and modules recovered from reference genes
Reads have been mapped to longest transcripts within Trinity clusters using Salmon in Step 4. We used the TPM values generated by Salmon for gene co-expression analysis.

First, extract TPM values from each sample
```
cut -f4 <XXX_longest_contig_salmon_map/quant.sf> ><XXX_tpm>
```
Next, combine TPM from each sample to one matrix
```
paste <multiple_XXX_tpm_files_from_last_step> >Trinity_longest_contig_salmon_TPM
```
Manually change the first line of the matrix to corresponding sample names. The matrix was used for the next analysis.

6.1 Gene co-expression analysis using software WGCNA
Conduct gene co-expression analysis using transcriptome composed of longest transcript and reference genome genes separately. Example R scripts were attached. Each gene co-expression module included its hub gene IDs.

6.2 Compare modules recovered from de novo assembly and reference genes
```
python evaluate_WGCNA_module.py <blat_output_file> <dir_modules>
```
The generated results were shown in Supplementary Table S7.