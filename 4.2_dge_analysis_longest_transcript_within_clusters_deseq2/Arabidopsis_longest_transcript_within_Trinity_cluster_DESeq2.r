library(DESeq2)
library("RColorBrewer")
library(tximport)
setwd("C:/cygwin64/home/chen_et_al_2018_benchmark_dge_r1/dge_analyses_longest_transcript/arab_dge_longest_transcript_mapping/1_arabidopsis_trinity_clusters.longest_transcript_salmon_map")
getwd()
samples = read.table(file = "samples_3.txt", sep = "\t", header = TRUE)
dir = "C:/cygwin64/home/chen_et_al_2018_benchmark_dge_r1/dge_analyses_longest_transcript/arab_dge_longest_transcript_mapping/1_arabidopsis_trinity_clusters.longest_transcript_salmon_map"
tx2gene <- read.table(file = "tx2gene", sep = "\t")

files <- file.path(dir, paste0(samples$run, "quant.sf"))
names(files) <- c("SRR3371309", "SRR3371320", "SRR3371331", "SRR3371342","SRR3371354", "SRR3371356")
txi.salmon <- tximport(files, type = "salmon", tx2gene = tx2gene,  dropInfReps=TRUE)
head(txi.salmon$counts)

# calculate mean expression level
table <- as.data.frame(txi.salmon$counts)
sum_rows <- rowSums(table)
mean_expression_gene <- sum(sum_rows)/length(files)/length(sum_rows)
# print the mean expression per gene per sample
mean_expression_gene


# calculate mean expression level
table <- as.data.frame(txi.salmon$counts)
sum_rows <- rowSums(table)
mean_expression_gene <- sum(sum_rows)/length(files)/length(sum_rows)
# print the mean expression per gene per sample
mean_expression_gene

condition <- factor(c (rep("control",3), rep("drought_5days", 3)))
condition <- relevel(condition, ref = "control")
sampleTable <- data.frame(condition)

dds <- DESeqDataSetFromTximport(txi.salmon, sampleTable, ~condition)


# 2 run DESeq, which is basically a wrapper for 3 step: estimateSizeFactors, estimateDispersions, nbinomWaldTest ( DESeq .ds)
dds <- DESeq(dds)   #The default log2foldchange did not use the standard log2 fold change methods. 
resultsNames(dds)

# 4 Differential expression analysis
#results() function lets you extract the base means across samples, moderated log2 fold changes, standard errors, test statistics, p-values and adjusted p-values
res <- results(dds)
resultsNames(dds)
res    # results: log2 fold change (MLE): condition hydro salt vs control 

# summarize results
summary(res)

# run the results with p value to 0.05 for different comparison
res05_35 <- results(dds, contrast=c("condition","control","drought_5days"), alpha =0.05)
res05_35_extracted <- res05_35[which(res05_35$pvalue <0.05 & abs(res05_35$log2FoldChange)>=2),] 

# 5 export data
write.csv(as.data.frame(res05_35), file="1_trinity_cluster_tximport_nofilter.txt")
write.csv(as.data.frame(res05_35_extracted), file="1_trinity_cluster_p_0.05_dgree_results_tximport_nofilter.txt")

# measure mean expression value for the DEG
res05_35_extracted_table <- as.data.frame(res05_35_extracted)
mean_expression_DEG <- sum(sum_rows[rownames(res05_35_extracted_table)])/length(rownames(res05_35_extracted_table))/length(files)
mean_expression_DEG

