#!/usr/bin/perl -w
#Extract fasta sequences according to sequence IDs
use strict;

my $idsfile = $ARGV[0]; #seq_id
my $seqfile = $ARGV[1]; #fasta_file
my $outfile = $ARGV[2]; #outfile_name
my %ids  = ();

open FILE, $idsfile;
while(<FILE>) {
  chomp;
  $ids{$_} += 1;
}
close FILE;

local $/ = "\n>";  # read by FASTA record

open FASTA, $seqfile;
while (<FASTA>) {
    chomp;
    my $seq = $_;
    my ($id) = $seq =~ /^>*(\S+)/;  # parse ID as first word in FASTA header
    if (exists($ids{$id})) {
            #   $seq =~ s/^>*.+\n//;  # remove FASTA header
            # $seq =~ s/\n//g;  # remove endlines
        #print ">"."$seq\n";
		open (OUTFILE,">>$outfile");
        print (OUTFILE ">"."$seq\n");
    }
}
close FASTA;
