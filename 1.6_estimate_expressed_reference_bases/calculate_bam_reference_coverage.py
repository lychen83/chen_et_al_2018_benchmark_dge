"""This script is used to calculate the proportion of bases of reference genes covered by reads. BAM file from samtools were used to calculate the depth"""

import sys,os
if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: calculate_bam_reference_coverage.py bam_depth"
		sys.exit(0)
	depth_file = open(sys.argv[1],'r')
	depth_file = depth_file.readlines()
	total_bases = 0
	covered_bases = 0
	not_covered_bases = 0
	for line in depth_file:
		if len(line) < 3: continue
		total_bases +=1
		spls = line.strip().split('\t')
		if float(spls[2]) >=15:
			covered_bases +=1
		if float(spls[2]) ==0:
			not_covered_bases +=1
	print "The total bases are", total_bases
	print "The covered bases are", covered_bases
	print "The proportion of covered bases are", float(covered_bases)/total_bases
	print "The proportion of not covered bases are", float(not_covered_bases)/total_bases
